const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
        /*archivos a mixear*/
        'resources/assets/css/bootstrap.css',
        'resources/assets/css/template.css',
        'resources/assets/css/responsive.css',
        'resources/assets/css/base-sizing.css',
        'resources/assets/css/niches/custom-restaurant.css',
        'resources/assets/css/custom.css',
    ]
    /*archivo resultante*/,'css/plantilla.css'
    /*archivos js*/
).scripts([
    'resources/js/template/jquery.js',
    'resources/js/template/bootstrap.js',

],'public/js/plantilla.js')
    .js(
'resources/js/app.js','public/js/app.js'
);
