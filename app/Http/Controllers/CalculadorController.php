<?php

namespace App\Http\Controllers;
use Response;
use Illuminate\Http\Request;

class CalculadorController extends Controller
{
    //
    public function calcular(Request $Request)
    {
        $int_estatura = (int)$Request->estatura;
        $int_peso = (int)$Request->peso;
        $int_estatura2 = $int_estatura* $int_estatura;
        $string_condicion = '';
        $string_mensaje = '';
        $decimal_imc = (float)($int_peso / $int_estatura2)*10000;
        if($decimal_imc <18){
            $string_condicion = "Peso bajo";
            $string_mensaje =  ". Necesario valorar signos de desnutricion";
        }
        elseif($decimal_imc>=18 && $decimal_imc <=24.99){
            $string_condicion = "Normal";
        }
        elseif($decimal_imc>=25 && $decimal_imc <=26.99){
            $string_condicion = "Sobrepeso";
            $string_mensaje =  ", se recomienda comprar el PLAN INCREDULOS O SOBREPESO";
        }
        elseif($decimal_imc==27){
            $string_condicion = "Obesidad";
            $string_mensaje =  ", se recomienda comprar el PLAN INCREDULOS U OBESIDAD";
        }
        elseif($decimal_imc>=27 && $decimal_imc<=29.99){
            $string_condicion = "Obesidad grado I";
            $string_mensaje =  ". Riesgo relativo alto para desarrollar enfermedades cardiovasculares, se recomienda comprar el PLAN INCREDULOS U OBESIDAD";
        }
        elseif($decimal_imc>=30 && $decimal_imc<=39.99){
            $string_condicion = "Obesidad grado II";
            $string_mensaje =  ". Riesgo relativo muy alto para el desarrollo de enfermedades cardiovasculares, se recomienda comprar el PLAN INCREDULOS U OBESIDAD";
        }
        elseif($decimal_imc>=40){
            $string_condicion = "Obesidad grado III Extrema o Morbida";
            $string_mensaje =  ". Riesgo relativo extremadamente alto para el desarrollo de enfermedades cardiovasculares, se recomienda comprar el PLAN INCREDULOS U OBESIDAD";
        }

        return Response::json([
            'imc' => $decimal_imc,
            'condicion' => $string_condicion,
            'mensaje' => $string_mensaje
        ]);
    }
}
