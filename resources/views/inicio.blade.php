<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Doctor Loyola</title>

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
    <link href="css/plantilla.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <app></app>
</div>
<script src="js/plantilla.js"></script>
<script src="js/app.js"></script>
</body>
</html>