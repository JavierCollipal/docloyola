
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './views/App';
import Inicio from './views/inicio';
import Formulario from './views/formulario';
import Calculadora from './views/calculadora'
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFacebookF} from '@fortawesome/free-brands-svg-icons';
import { faPhone } from  '@fortawesome/free-solid-svg-icons';
import { faInfoCircle} from  '@fortawesome/free-solid-svg-icons';
import { faSignInAlt } from  '@fortawesome/free-solid-svg-icons';
import { faSearch} from  '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';



// require styles
import 'swiper/dist/css/swiper.css';
import VueSpinners from 'vue-spinners'

Vue.use(VueSpinners);
Vue.use(VueMaterial);
Vue.use(VueAwesomeSwiper);
Vue.use(BootstrapVue);
Vue.use(VueRouter);
library.add(faFacebookF);
library.add(faPhone);
library.add(faInfoCircle);
library.add(faSignInAlt);

library.add(faSearch);

Vue.component('font-awesome-icon', FontAwesomeIcon);


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'inicio',
            component: Inicio
        },
        {
            path: '/formulario',
            name: 'formulario',
            component: Formulario
        },
        {
            path: '/calculadora',
            name: 'calculadora',
            component: Calculadora
        }
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
